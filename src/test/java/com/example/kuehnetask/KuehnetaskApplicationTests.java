package com.example.kuehnetask;

import com.example.kuehnetask.model.BitcoinPrice;
import com.example.kuehnetask.service.BitcoinService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

@SpringBootTest
@ActiveProfiles("test")
class KuehnetaskApplicationTests {
    private static final String EURO_CODE = "EUR";

    @Autowired
    private BitcoinService bitcoinService;

    @Test
    void contextLoads() {
    }

    @Test
    void should_get_bitcoin_price_correctly() {
        BitcoinPrice price = bitcoinService.getPrice(EURO_CODE);

        Assertions.assertEquals(EURO_CODE, price.getCurrency().getCurrencyCode());
        Assertions.assertTrue(price.getCurrentRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(price.getLowestRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(price.getHighestRate().compareTo(BigDecimal.ZERO) > 0);

    }

}
