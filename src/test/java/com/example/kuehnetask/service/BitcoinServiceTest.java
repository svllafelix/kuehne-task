package com.example.kuehnetask.service;

import com.example.kuehnetask.client.CoindeskClient;
import com.example.kuehnetask.dto.coindesk.CurrentPrice;
import com.example.kuehnetask.dto.coindesk.CurrentPriceByCurrency;
import com.example.kuehnetask.dto.coindesk.IntervalClosePrice;
import com.example.kuehnetask.exception.UnknownCurrencyException;
import com.example.kuehnetask.model.BitcoinPrice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
public class BitcoinServiceTest {
    private static final String EURO_CODE = "EUR";
    private static final Currency EURO_CURRENCY = Currency.getInstance("EUR");

    private static final LocalDate START_DATE = LocalDate.now().minusDays(2);
    private static final LocalDate END_DATE = LocalDate.now().minusDays(1);

    @Mock
    private CoindeskClient coindeskClient;

    private BitcoinService bitcoinService;

    @BeforeEach
    public void setup() {
        bitcoinService = new BitcoinService(coindeskClient, 2);
    }

    @Test
    public void getPrice_should_throw_exception_if_currency_does_not_exist() {
        // given
        String faultyCurrencyCode = "XYZ";

        // when
        Executable getCurrentPrice = () -> bitcoinService.getPrice(faultyCurrencyCode);

        // then
        Assertions.assertThrows(UnknownCurrencyException.class, getCurrentPrice);
    }

    @Test
    public void getPrice_should_return_current_value_correctly() {
        // given
        mockCoindeskCall();

        // when
        BitcoinPrice price = bitcoinService.getPrice(EURO_CODE);

        // then
        Assertions.assertEquals(EURO_CURRENCY, price.getCurrency());
        Assertions.assertEquals(BigDecimal.valueOf(1000), price.getCurrentRate());
    }

    @Test
    public void getPrice_should_return_current_lowest_correctly() {
        // given
        mockCoindeskCall();

        // when
        BitcoinPrice price = bitcoinService.getPrice(EURO_CODE);

        // then
        Assertions.assertEquals(EURO_CURRENCY, price.getCurrency());
        Assertions.assertEquals(BigDecimal.valueOf(500), price.getLowestRate());
    }

    @Test
    public void getPrice_should_return_current_highest_correctly() {
        // given
        mockCoindeskCall();

        // when
        BitcoinPrice price = bitcoinService.getPrice(EURO_CODE);

        // then
        Assertions.assertEquals(EURO_CURRENCY, price.getCurrency());
        Assertions.assertEquals(BigDecimal.valueOf(2000), price.getHighestRate());
    }

    private void mockCoindeskCall() {
        CurrentPrice mockPrice = getMockPrice();
        IntervalClosePrice intervalClosePrice = getMockIntervalPrice();
        given(coindeskClient.getCurrentPrice(eq(EURO_CODE))).willReturn(mockPrice);
        given(coindeskClient.getIntervalClosePrice(eq(START_DATE), eq(END_DATE), eq(EURO_CODE))).willReturn(intervalClosePrice);
    }

    private CurrentPrice getMockPrice() {
        CurrentPrice currentPrice = new CurrentPrice();
        CurrentPriceByCurrency euroPrice = new CurrentPriceByCurrency();
        euroPrice.setCode(EURO_CODE);
        euroPrice.setRate(BigDecimal.valueOf(1000));

        Map<String, CurrentPriceByCurrency> rateMap = new HashMap<>();
        rateMap.put(EURO_CODE, euroPrice);
        currentPrice.setBpi(rateMap);

        return currentPrice;
    }

    private IntervalClosePrice getMockIntervalPrice() {
        IntervalClosePrice intervalClosePrice = new IntervalClosePrice();
        Map<LocalDate, BigDecimal> rateMap = new HashMap<>();
        rateMap.put(LocalDate.now().minusDays(2), BigDecimal.valueOf(2000));
        rateMap.put(LocalDate.now().minusDays(1), BigDecimal.valueOf(500));
        intervalClosePrice.setBpi(rateMap);

        return intervalClosePrice;
    }
}
