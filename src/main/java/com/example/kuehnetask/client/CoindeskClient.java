package com.example.kuehnetask.client;

import com.example.kuehnetask.dto.coindesk.CurrentPrice;
import com.example.kuehnetask.dto.coindesk.IntervalClosePrice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;

@FeignClient(value = "coindesk", url = "${coindesk.server}")
public interface CoindeskClient {

    @GetMapping(value = "/currentprice/{currencyCode}.json")
    CurrentPrice getCurrentPrice(@PathVariable String currencyCode);

    @GetMapping(value = "/historical/close.json?start={start}&end={end}&currency={currencyCode}")
    IntervalClosePrice getIntervalClosePrice(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
                                             @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end,
                                             @PathVariable String currencyCode);
}
