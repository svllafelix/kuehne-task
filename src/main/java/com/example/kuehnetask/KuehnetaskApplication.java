package com.example.kuehnetask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class KuehnetaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(KuehnetaskApplication.class, args);
    }
}
