package com.example.kuehnetask.service;

import com.example.kuehnetask.exception.UnknownCurrencyException;
import com.example.kuehnetask.client.CoindeskClient;
import com.example.kuehnetask.dto.coindesk.IntervalClosePrice;
import com.example.kuehnetask.model.BitcoinPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Currency;

@Service
public class BitcoinService {
    private final CoindeskClient coindeskClient;
    private final Integer queryInterval;

    public BitcoinService(@Autowired CoindeskClient coindeskClient, @Value("${bitcoin.interval}") Integer queryInterval) {
        this.coindeskClient = coindeskClient;
        this.queryInterval = queryInterval;
    }

    public BitcoinPrice getPrice(String currencyCode) {
        Currency currency = getCurrency(currencyCode);
        BigDecimal currentPrice = coindeskClient.getCurrentPrice(currencyCode).getBpi().get(currencyCode).getRate();
        IntervalClosePrice intervalClosePrice = coindeskClient.getIntervalClosePrice(LocalDate.now().minusDays(queryInterval),
                                                                                     LocalDate.now().minusDays(1),
                                                                                     currencyCode);
        Collection<BigDecimal> values = new ArrayList<>(intervalClosePrice.getBpi().values());
        values.add(currentPrice);
        BigDecimal lowest = findLowest(values);
        BigDecimal highest = findHighest(values);
        return new BitcoinPrice(currency, currentPrice, lowest, highest);
    }

    private BigDecimal findLowest(Collection<BigDecimal> values) {
        return values.stream().min(BigDecimal::compareTo).orElseThrow();
    }

    private BigDecimal findHighest(Collection<BigDecimal> values) {
        return values.stream().max(BigDecimal::compareTo).orElseThrow();
    }

    private Currency getCurrency(String currencyCode) throws UnknownCurrencyException {
        try {
            return Currency.getInstance(currencyCode);
        } catch (IllegalArgumentException e) {
            throw new UnknownCurrencyException(currencyCode);
        }
    }
}
