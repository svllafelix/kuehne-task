package com.example.kuehnetask.exception;

public class UnknownCurrencyException extends RuntimeException {
    private final static String MESSAGE = "Unknown currency code: %s";

    public UnknownCurrencyException(String faultyCurrencyCode) {
        super(String.format(MESSAGE, faultyCurrencyCode));
    }
}
