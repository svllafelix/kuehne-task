package com.example.kuehnetask.dto.coindesk;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

@Data
public class IntervalClosePrice {
    private Time time;
    private String disclaimer;
    private Map<LocalDate, BigDecimal> bpi;
}
