package com.example.kuehnetask.dto.coindesk;

import lombok.Data;

@Data
public class Time {
    private String updated;
    private String updatedISO;
    private String updateduk;
}
