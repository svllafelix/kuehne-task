package com.example.kuehnetask.dto.coindesk;

import lombok.Data;

import java.util.Map;

@Data
public class CurrentPrice {
    private Time time;
    private String disclaimer;
    private Map<String, CurrentPriceByCurrency> bpi;
}
