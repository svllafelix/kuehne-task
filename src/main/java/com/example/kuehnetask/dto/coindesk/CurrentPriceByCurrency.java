package com.example.kuehnetask.dto.coindesk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CurrentPriceByCurrency {
    private String code;
    @JsonProperty("rate_float")
    private BigDecimal rate;
    private String description;
}
