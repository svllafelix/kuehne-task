package com.example.kuehnetask.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;

@RequiredArgsConstructor
@Getter
public class BitcoinPrice {
    private final Currency currency;
    private final BigDecimal currentRate;
    private final BigDecimal lowestRate;
    private final BigDecimal highestRate;
}
