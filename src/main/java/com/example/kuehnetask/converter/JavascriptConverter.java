package com.example.kuehnetask.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

@Component
public class JavascriptConverter extends AbstractJackson2HttpMessageConverter {
    public JavascriptConverter(@Autowired ObjectMapper objectMapper) {
        super(objectMapper, new MediaType("application", "javascript"));
    }
}

