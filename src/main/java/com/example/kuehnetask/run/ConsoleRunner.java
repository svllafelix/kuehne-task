package com.example.kuehnetask.run;

import com.example.kuehnetask.model.BitcoinPrice;
import com.example.kuehnetask.service.BitcoinService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.stream.Stream;

@Profile("!test")
@Component
@RequiredArgsConstructor
public class ConsoleRunner implements CommandLineRunner {
    private final BitcoinService bitcoinService;

    @Override
    public void run(String... args) {
        Scanner in = new Scanner(System.in);
        boolean continueFlag = true;
        while (continueFlag) {
            System.out.println("Please enter the currency code");
            String currencyCode = in.nextLine().toUpperCase();

            try {
                BitcoinPrice price = bitcoinService.getPrice(currencyCode);
                System.out.printf("The bitcoin price in currency: %s%ncurrent: %s%nlowest: %s%nhighest: %s%n",
                        currencyCode, price.getCurrentRate(), price.getLowestRate(), price.getHighestRate());
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("Do you want to continue (Y)es/(N)o");
            String continueResponse = in.nextLine();
            continueFlag = Stream.of("Y", "Yes").anyMatch(yValue -> yValue.equalsIgnoreCase(continueResponse));
        }
    }
}
