FROM adoptopenjdk/openjdk11
COPY target/kuehnetask-0.0.1-SNAPSHOT.jar /app.jar
CMD ["java", "-jar", "/app.jar"]