# Kuehne Nagel Task
This is the implementation of the kuehne nagel task. It is a Spring boot command-line application.

### Installation
Installation and start by maven
```
mvn clean install
mvn spring-boot:run
```

Running by docker
```
docker run -it sullafelix/kuehnetask:latest
```

### Configuration
It is possible to change the number of days in the past to consider while finding lowest/highest value

```yml
bitcoin:
  interval: 30
```

### Usage
The program starts by prompting for a currency code. 
After the currency code is input it retrieves the current bitcoin price and the lowest/highest 
prices in the latest X days(configurable as mentioned above).

```
Please enter the currency code
try
The bitcoin price in currency: TRY
current: 674503.8958
lowest: 591314.9576
highest: 773434.0933
Do you want to continue (Y)es/(N)o
```
After the price has been displayed the program asks the user if he/she wants to continue
with another query or stop the program. Answers y or Yes (case-insensitive) will 
continue and any other input will cause the program to stop.